/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswing;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

/**
 *
 * @author 66955
 */
public class JavaJProgressBar extends JFrame{

    JProgressBar jb;
    int i = 0, num = 0;

    JavaJProgressBar() {
        jb = new JProgressBar(0, 2000);
        jb.setBounds(40, 40, 160, 30);
        jb.setValue(0);
        jb.setStringPainted(true);
        add(jb);
        setSize(250, 150);
        setLayout(null);
    }

    public void iterate() {
        while (i <= 2000) {
            jb.setValue(i);
            i = i + 20;
            try {
                Thread.sleep(150);
            } catch (Exception e) {
            }
        }
    }

    public static void main(String[] args) {
        JavaJProgressBar m = new JavaJProgressBar();
        m.setVisible(true);
        m.iterate();
    }
}
